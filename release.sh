#!/bin/bash

commitCount=$(git rev-list HEAD --count)

git tag -a "$commitCount" HEAD -m "Android Release $commitCount"
git push origin master:master --tags
